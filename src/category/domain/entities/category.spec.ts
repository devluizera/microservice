import Category, { CategoryProperties } from './category';
import { omit } from 'lodash';
import { validate as uuuiValidate } from 'uuid';

describe('Category Unit Tests', () => {
    test('constructor of category', () => {
        let category = new Category({ name: 'Movie' });
        let props = omit(category.props, 'created_at');
        expect(props).toStrictEqual({
            name: 'Movie',
            description: null,
            is_active: true,
        })
        expect(category.props.created_at).toBeInstanceOf(Date);

        category = new Category({
            name: 'Movie 2',
            description: 'some description',
            is_active: false
        })
        let created_at = new Date();
        expect(category.props).toStrictEqual({
            name: 'Movie 2',
            description: 'some description',
            is_active: false,
            created_at,
        })

        category = new Category({
            name: 'Movie 3',
            description: 'other description',
        })
        expect(category.props).toMatchObject({
            name: 'Movie 3',
            is_active: true,
        })
    });

    test('id field', () => {
        type CategoryData = { props: CategoryProperties, id?: string };
        const data: CategoryData[] = [
            { props: { name: 'Movie' } },
            { props: { name: 'Movie' }, id: null },
            { props: { name: 'Movie' }, id: undefined },
            { props: { name: 'Movie' }, id: '606835bd-2933-4701-a934-26536d25dcae' },
        ];

        data.forEach((i: CategoryData) => {
            const category = new Category(i.props, i.id);
            expect(category.id).not.toBeNull();
            expect(uuuiValidate(category.id)).toBeTruthy();
        });
    });

    test('getter of name prop', () => {
        let category = new Category({ name: 'Movie 3' });
        expect(category.name).toBe('Movie 3');
    });

    test('getter and setter of description prop', () => {
        let category = new Category({ name: 'Movie' });
        expect(category.description).toBeNull();

        category = new Category({ name: 'Movie', description: 'some description' });
        expect(category.description).toBe('some description');

        category['description'] = 'other description';
        expect(category.description).toBe('other description');

        category['description'] = undefined;
        expect(category.description).toBeNull();

        category['description'] = null;
        expect(category.description).toBeNull();
    })

    test('getter and setter of is active prop', () => {
        let category = new Category({
            name: 'Movie',
        });
        expect(category.is_active).toBeTruthy();

        category['is_active'] = false;
        expect(category.is_active).toBeFalsy();
    });

    test('getter of created_at prop', () => {
        let category = new Category({
            name: 'Movie',
        });
        expect(category.created_at).toBeInstanceOf(Date);

        let created_at = new Date();

        category = new Category({
            name: 'Movie',
            created_at,
        });

        expect(category.created_at).toBe(created_at);
    });
});